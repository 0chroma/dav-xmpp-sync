FROM python:3.8

ENV CARDDAV_URL https://example.com/bob/Contacts.vcf/
ENV CARDDAV_USERNAME bob
ENV CARDDAV_PASSWORD bobspassword
ENV CONTACT_SOURCE carddav
ENV VCARD_FILE /import/contacts.vcf
ENV XMPP_JID bob@example.com
ENV XMPP_PASSWORD bobspassword
ENV SYNC_NUMBER_TYPE cell
ENV SYNC_NUMBER_PREFIX +1
ENV SYNC_GATEWAY_DOMAIN cheogram.com

COPY requirements.txt /tmp/requirements.txt
RUN pip install -r /tmp/requirements.txt
RUN rm /tmp/requirements.txt

RUN adduser --system --shell /bin/sh --home /app app
COPY sync.py /app/sync
COPY docker-run.sh /app/docker-run
RUN chmod 755 /app/sync /app/docker-run

USER app

ENTRYPOINT ["/app/docker-run"]


