#!/bin/sh

cd /app

if [ ! -f /app/config.yaml ]; then

  case "$CONTACT_SOURCE" in
    "carddav")
    cat <<EOF > /app/config.yaml
carddav:
  url: $CARDDAV_URL
  username: $CARDDAV_USERNAME
  password: $CARDDAV_PASSWORD
xmpp:
  jid: $XMPP_JID
  password: $XMPP_PASSWORD
sync:
  number_type: $SYNC_NUMBER_TYPE
  number_prefix: "$SYNC_NUMBER_PREFIX"
  gateway_domain: $SYNC_GATEWAY_DOMAIN
EOF
;;
   "file")
    cat <<EOF > /app/config.yaml
vcards:
  file: "$VCARD_FILE"
xmpp:
  jid: $XMPP_JID
  password: $XMPP_PASSWORD
sync:
  number_type: $SYNC_NUMBER_TYPE
  number_prefix: "$SYNC_NUMBER_PREFIX"
  gateway_domain: $SYNC_GATEWAY_DOMAIN
EOF
;;
  *)
  echo "Error: Unknown Contact Source $CONTACT_SOURCE"
  exit 2
  ;;
  esac
fi

exec /app/sync "$@"