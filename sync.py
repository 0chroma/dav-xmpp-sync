#!/usr/bin/env python
# Written by Sumit Khanna
# https://battlepenguin.com/tech/moving-my-phone-number-from-google-hangouts-voice-to-an-sip-xmpp-service/
# License: AGPLv3
import xmpp
import requests
from requests.auth import HTTPBasicAuth
import re
import logging
from rich.logging import RichHandler
from rich.table import Table
from rich.console import Console
import yaml
import click
import sys


# Temporary patch until this is fixed:
# https://github.com/xmpppy/xmpppy/issues/52#issuecomment-993084240
def _C_monkey_patch(some):
    if sys.version_info >= (3, 0, 0):
        some = [x.encode('utf-8') if isinstance(x, str) else x for x in some]
        return b':'.join(some)
    return ':'.join(map(str, some))


xmpp.auth.C = _C_monkey_patch


logging.basicConfig(format='%(message)s', datefmt="[%X]", handlers=[RichHandler()])
log = logging.getLogger("xmpp-dav-sync")


def normalize_phone_numbers(number):
    return re.sub(r'[\s\(\)-]', '', number)


def read_vcards(config):
    """Step 1: called from main to determine cards source and read them"""
    if 'vcards' in config and 'carddav' in config:
        log.error('You may specify either vcards or carddav, not both.')
        sys.exit(3)

    if 'carddav' in config:
        dav_config = config['carddav']
        return read_caldav(dav_config['url'], dav_config['username'], dav_config['password'])
    elif 'vcards' in config:
        return read_vcardfile(config['vcards']['file'])
    else:
        log.error('No contact retrieval defined in config. You must have a vcards or carddav section.')
        sys.exit(3)


def read_caldav(url, username, password):
    """Step 2 called from read_vcards to read from CardDav server with authentication"""
    log.info(f'Requesting vCards From {url}')
    resp = requests.get(url, auth=HTTPBasicAuth(username, password))
    return split_all_vcards(resp.text)


def read_vcardfile(file):
    """Step 2 called from read_vcards to read from file"""
    log.info(f'Loading vCards from {file}')
    with open(file, 'r') as fd:
        return split_all_vcards(fd.read())


def split_all_vcards(vcards):
    """Step 3: Split a long string from a file or HTTP request body into individual vcards"""
    if 'BEGIN:VCARD' not in vcards:
        log.error('No VCARDs found')
        log.debug(f'Document: {vcards}')
        sys.exit(2)
    else:
        card_split = re.split(r'END:VCARD(\r\n|\r|\n)BEGIN:VCARD', vcards)
        # remove \r\n elements
        cards = [parsecard(c) for c in card_split if c.strip() != '']
        return cards


def parsecard(card_string):
    """Step 4: Parse Individual Cards"""
    lines = card_string.splitlines()
    uid = [p for p in lines if p.startswith('UID')][0].split(':')[1]
    raw_numbers = [m for m in lines if m.startswith('TEL')]
    raw_cats = ([n for n in lines if n.startswith('CATEGORIES')] or [":Other"])[0].split(':')[1]
    cats = [c.strip() for c in raw_cats.split(',')]
    full_name = [o for o in lines if o.startswith('FN')][0].split(':')[1]
    numbers = {}
    for r in raw_numbers:
        num_type_match = re.search(r'TYPE=(.*?)[:;]', r)
        num_type = num_type_match.group(1) if num_type_match is not None else 'UNKNOWN'
        num = normalize_phone_numbers(r.split(':')[1])
        if num_type.lower() not in numbers:
            numbers[num_type.lower()] = []
        numbers[num_type.lower()].append(num)
    return {'uid': uid, 'numbers': numbers, 'full_name': full_name, 'tags': cats}


def print_cards(cards):
    table = Table(title='Contacts')
    table.add_column('Name')
    table.add_column('Numbers')
    table.add_column('Categories')
    for c in cards:
        nums = ', '.join('{}:{}'.format(k, v) for k, v in c['numbers'].items())
        table.add_row(c['full_name'], nums, ','.join(c['tags']))
    console = Console()
    console.print(table)


def print_cat_counts(cats):
    table = Table(title='Categories')
    table.add_column('Category')
    table.add_column('Num Entries')
    for c in cats.keys():
        table.add_row(c, str(len(cats[c])))
    console = Console()
    console.print(table)


def filter_cards_with_numbers(all_cards, number_type):
    phone_cards = []
    for a in all_cards:
        if number_type not in a['numbers']:
            found_cats = list(a['numbers'].keys())
            if len(found_cats) > 0:
                log.warning(f"{a['full_name']} has no phone numbers of type {number_type}. Found: {list(a['numbers'].keys())}")
            else:
                log.warning(f"{a['full_name']} has no phone numbers")
        else:
            phone_cards.append(a)
    return phone_cards


def cards_by_category(cards):
    by_category = {}
    for c in cards:
        for t in c['tags']:
            if t not in by_category:
                by_category[t] = []
            by_category[t].append(c)
    return by_category


def cards_by_phone_number(cards, number_type_filter, number_prefix_filter):
    retval = {}
    for c in cards:
        if number_type_filter in c['numbers']:
            if number_prefix_filter is None or c['numbers'][number_type_filter][0].startswith(number_prefix_filter):
                retval[c['numbers'][number_type_filter][0]] = {'name': c['full_name'], 'groups': c['tags']}
    return retval


def load_config(config_file):
    with open(config_file, 'r') as fd:
        y_config = yaml.safe_load(fd)
    return y_config


def new_groups(dav_groups, xmpp_groups, managed_groups):
    """Resolves CardDav groups with those that are managed.
       We want to managed adding and deleting from categories defined in CardDav,
       but not remove people from XMPP groups, such as if someone creates a group
       such as "Recent" or "Favourites," to be managed outside their contacts.
    """
    unmanaged = [x for x in xmpp_groups if x not in managed_groups]
    return dav_groups + unmanaged


def update_roster(dry_run, xmpp_conn, cards_by_number, gateway_domain, managed_groups):

    at_gw_domain = f'@{gateway_domain}'
    dry_msg = '(DRYRUN) ' if dry_run else ''

    roster_obj = xmpp_conn.getRoster()
    raw_roster = roster_obj.getRawRoster()
    filtered_roster = {k: v for k, v in raw_roster.items() if k.endswith(gateway_domain)}

    for number, info in cards_by_number.items():
        new_jid = f'{number}{at_gw_domain}'
        if new_jid not in filtered_roster:
            log.info(f"{dry_msg}Adding {info['name']} ({number})")
            if not dry_run:
                roster_obj.Subscribe(new_jid)
                roster_obj.Authorize(new_jid)
        else:
            log.debug(f"Contact {info['name']} ({number}) exists on XMPP Server")

    for num, data in filtered_roster.items():
        if data['subscription'] != 'both':
            log.info(f"{dry_msg}Subscribing {num}")
            if not dry_run:
                roster_obj.Subscribe(num)
                roster_obj.Authorize(num)

    for jid, data in filtered_roster.items():
        if jid.endswith(at_gw_domain):
            phone = jid.rstrip(at_gw_domain)
            if phone not in cards_by_number:
                log.info(f'{phone} Not Found in Address Book')
            else:
                cur_card = cards_by_number[phone]
                updated_name = cur_card['name']
                update_groups = new_groups(cur_card['groups'], data['groups'], managed_groups)
                do_update = False
                if cards_by_number[phone]['name'] != data['name']:
                    log.info(f'{dry_msg}Updating {phone}. Setting name to {updated_name}')
                    do_update = True
                if not set(update_groups) == set(data['groups']):
                    log.info(f"{dry_msg}Updating groups for {cur_card['name']} to {update_groups}")
                    do_update = True
                if do_update and not dry_run:
                    roster_obj.setItem(jid, name=updated_name, groups=update_groups)


def xmpp_client(x_config):
    jid = xmpp.protocol.JID(x_config['jid'])
    con = xmpp.Client(server=jid.getDomain(), debug=[])
    con.connect()
    con.auth(jid.getNode(), x_config['password'])
    return con


@click.group()
@click.option('--config', default='config.yaml', help='Configuration YAML File')
@click.option('--dry-run', is_flag=True, default=False, help='Do not perform updates')
# Not yet implemented
# @click.option('--deletes', is_flag=True, default=False, help='Delete phone numbers in managed groups that are not in contacts')
@click.option('--verbose/--silent', default=False, help='Additional diagnostics and logging')
@click.pass_context
def main(ctx, config, dry_run, verbose):
    ctx.ensure_object(dict)
    ctx.obj['config'] = load_config(config)
    ctx.obj['dry_run'] = dry_run

    if verbose:
        log.setLevel(logging.DEBUG)
    else:
        log.setLevel(logging.INFO)


@main.command()
@click.pass_context
def sync(ctx):
    # dav_config = ctx.obj['config']['carddav']
    syn_config = ctx.obj['config']['sync']
    x_config = ctx.obj['config']['xmpp']

    all_cards = read_vcards(ctx.obj['config'])
    with_number = filter_cards_with_numbers(all_cards, syn_config['number_type'])
    by_phone_number = cards_by_phone_number(with_number, syn_config['number_type'], syn_config.get('number_prefix', None))

    managed_categories = list(cards_by_category(all_cards).keys())
    log.info(f'Managed Groups {managed_categories}')

    con = xmpp_client(x_config)
    update_roster(ctx.obj['dry_run'], con, by_phone_number, syn_config['gateway_domain'], managed_categories)


@main.command()
@click.pass_context
def contacts(ctx):
    # dav_config = ctx.obj['config']['carddav']
    all_cards = read_vcards(ctx.obj['config'])
    print_cards(all_cards)


@main.command()
@click.pass_context
def categories(ctx):
    all_cards = read_vcards(ctx.obj['config'])
    card_cats = cards_by_category(all_cards)
    print_cat_counts(card_cats)


@main.command()
@click.pass_context
def roster(ctx):
    x_config = ctx.obj['config']['xmpp']
    syn_config = ctx.obj['config']['sync']
    con = xmpp_client(x_config)
    roster_obj = con.getRoster()
    raw_roster = roster_obj.getRawRoster()

    table = Table(title='XMPP Roster')
    table.add_column('Name')
    table.add_column('Numbers')
    table.add_column('Subscription')
    table.add_column('Groups')

    for num, data in raw_roster.items():

        g = data['groups'] if data['groups'] else []

        # Show the gateway domain transport account
        if num.endswith(syn_config['gateway_domain']) and num != syn_config['gateway_domain']:
            n = num.rstrip(f"@{syn_config['gateway_domain']}")
        else:
            n = num

        if data['subscription'] == 'both':
            sub = '[green]both[/green]'
        elif data['subscription'] == 'none':
            sub = '[red]none[/red]'
        else:
            sub = data['subscription']

        table.add_row(data['name'], n, sub, ','.join(g))
    console = Console()
    console.print(table)


if __name__ == '__main__':
    main()
