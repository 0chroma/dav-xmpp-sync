# Written by Sumit Khanna
# https://battlepenguin.com/tech/moving-my-phone-number-from-google-hangouts-voice-to-an-sip-xmpp-service/
# License: AGPLv3
import unittest
import pytest
from sync import parsecard, normalize_phone_numbers, filter_cards_with_numbers, \
    new_groups, read_caldav, read_vcards
import responses
import tempfile
import os


class HelperFunctionsTests(unittest.TestCase):

    def setUp(self):
        self.managed_groups = ['Family', 'Friends', 'Cincinnati']

    def test_add_no_groups(self):
        dav_groups = []
        xmpp_groups = []
        result = new_groups(dav_groups, xmpp_groups, self.managed_groups)
        self.assertEqual(result, [])

    def test_add_some_managed_groups(self):
        dav_groups = ['Family', 'Cincinnati']
        xmpp_groups = []
        result = new_groups(dav_groups, xmpp_groups, self.managed_groups)
        self.assertEqual(result, dav_groups)

    def test_add_some_managed_some_unmanaged_groups(self):
        dav_groups = ['Family', 'Cincinnati']
        xmpp_groups = ['Family', 'Recent']
        result = new_groups(dav_groups, xmpp_groups, self.managed_groups)
        self.assertEqual(result, ['Family', 'Cincinnati', 'Recent'])

    def test_remove_some_managed_groups(self):
        dav_groups = ['Cincinnati']
        xmpp_groups = ['Family', 'Cincinnati', 'Friends']
        result = new_groups(dav_groups, xmpp_groups, self.managed_groups)
        self.assertEqual(result, ['Cincinnati'])

    def test_remove_some_managed_groups_some_unmanaged(self):
        dav_groups = ['Cincinnati']
        xmpp_groups = ['Family', 'Cincinnati', 'InternetFriends']
        result = new_groups(dav_groups, xmpp_groups, self.managed_groups)
        self.assertEqual(result, ['Cincinnati', 'InternetFriends'])


class CardDavTests(unittest.TestCase):

    one_number_card = """VERSION:3.0
UID:dbbc47b6-c2da-481d-9f14-8cb2d821217d
CATEGORIES: United States
EMAIL;TYPE=INTERNET,HOME:mike@example.com
EMAIL;TYPE=INTERNET:mike@example.net
FN:Mike FooMan
N:FooMan;Mike;;;
TEL;TYPE=CELL:+1 423-555-6668
item1.X-ABLABEL:PROFILE
X-RADICALE-NAME:4E8637F4-3CE1E309-C15998AF.vcf
    """

    no_number_card = """VERSION:3.0
UID:09e43f1c-46c1-47b4-80f9-11d58aa4a7bc
ADR;TYPE=HOME:;;9999 S Odessa St;Nowhere;NY;00000;
CATEGORIES: United States,Seattle
EMAIL;TYPE=INTERNET:x@gmail.com
FN:Travis TheDude
N:TheDude;Travis;;;
X-RADICALE-NAME:4E6FD17D-54BF7751-A6A75C1F.vcf
"""

    multi_number_card = """VERSION:3.0
UID:ccdc36a0-ce46-4d79-9550-269ff66fdc4c
CATEGORIES: United States
EMAIL;TYPE=INTERNET,HOME:x@msn.com
FN:Daniel TheBee
N:TheBee;Daniel;;;
TEL;TYPE=CELL:+1 423-555-1111
TEL;TYPE=HOME:+1 706-555-2222
TEL;TYPE=WORK:+1 314-555-3333
X-RADICALE-NAME:614D1B75-713A2BB9-4714BE83.vcf
"""

    def setUp(self):

        card_string_lst = [CardDavTests.one_number_card,
                           CardDavTests.no_number_card,
                           CardDavTests.multi_number_card]

        self.all_cards = [parsecard(c) for c in card_string_lst]

        self.vcard_tmpfile = tempfile.mkstemp(suffix='.vcf')
        vcards_string = ''.join([f'BEGIN:VCARD\n{f}END:VCARD\n' for f in card_string_lst])
        with open(self.vcard_tmpfile[1], 'w') as fd:
            fd.write(vcards_string)

    def tearDown(self):
        os.unlink(self.vcard_tmpfile[1])

    def test_bad_config_both_vcards_carddav_sections(self):
        with self.assertLogs('xmpp-dav-sync', level='ERROR') as cm:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                read_vcards({'carddav': {}, 'vcards': {}})
            self.assertEqual(pytest_wrapped_e.type, SystemExit)
            self.assertEqual(pytest_wrapped_e.value.code, 3)
            self.assertEqual(cm.output, ['ERROR:xmpp-dav-sync:You may specify either vcards or carddav, not both.'])

    def test_bad_config_no_contact_retrieval(self):
        with self.assertLogs('xmpp-dav-sync', level='ERROR') as cm:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                read_vcards({'xmpp': {}, 'sync': {}})
            self.assertEqual(pytest_wrapped_e.type, SystemExit)
            self.assertEqual(pytest_wrapped_e.value.code, 3)
            self.assertEqual(cm.output, ['ERROR:xmpp-dav-sync:No contact retrieval defined in config. You must have a vcards or carddav section.'])

    def test_vcard_file_load(self):
        cards = read_vcards({'vcards': {'file': self.vcard_tmpfile[1]}})
        self.assertEqual(len(cards), 3)

    @responses.activate
    def test_invalid_carddav(self):
        url = 'http://example.com/Contacts.vcf'
        responses.add(responses.GET, url, body='404 Not Found', status=404)

        with self.assertLogs('xmpp-dav-sync', level='INFO') as cm:
            with pytest.raises(SystemExit) as pytest_wrapped_e:
                read_caldav(url, 'user', 'pass')
            self.assertEqual(pytest_wrapped_e.type, SystemExit)
            self.assertEqual(pytest_wrapped_e.value.code, 2)
            self.assertEqual(cm.output, ['INFO:xmpp-dav-sync:Requesting vCards From http://example.com/Contacts.vcf',
                                         'ERROR:xmpp-dav-sync:No VCARDs found'])

    @responses.activate
    def test_valid_carddav(self):
        card_body = ''.join([f'BEGIN:VCARD\n{f}END:VCARD\n' for f in [CardDavTests.one_number_card,
                                                                      CardDavTests.no_number_card,
                                                                      CardDavTests.multi_number_card]])

        url = 'http://example.com/Contacts.vcf'
        responses.add(responses.GET, url, body=card_body, status=200)
        cards = read_caldav(url, 'user', 'pass')
        self.assertEqual(len(cards), 3)

    def test_one_num_card(self):
        one_num = parsecard(CardDavTests.one_number_card)
        self.assertEqual(one_num, {'uid':
                                   'dbbc47b6-c2da-481d-9f14-8cb2d821217d',
                                   'numbers': {'cell': ['+14235556668']}, 'full_name': 'Mike FooMan', 'tags': ['United States']})

    def test_no_num_card(self):
        no_num = parsecard(CardDavTests.no_number_card)
        self.assertEqual(no_num, {'uid':
                                  '09e43f1c-46c1-47b4-80f9-11d58aa4a7bc',
                                  'numbers': {}, 'full_name': 'Travis TheDude', 'tags': ['United States', 'Seattle']})

    def test_multi_num_card(self):
        multi_num = parsecard(CardDavTests.multi_number_card)
        self.assertEqual(multi_num, {'uid':
                                     'ccdc36a0-ce46-4d79-9550-269ff66fdc4c',
                                     'numbers': {'cell': ['+14235551111'], 'home': ['+17065552222'], 'work': ['+13145553333']}, 'full_name': 'Daniel TheBee', 'tags': ['United States']})

    def test_normalize_phone_numbers(self):
        self.assertEqual(normalize_phone_numbers('+1 615 555-1248'), '+16155551248')
        self.assertEqual(normalize_phone_numbers('+64 027 6666'), '+640276666')
        self.assertEqual(normalize_phone_numbers('+4 123-45-67 89'), '+4123456789')
        self.assertEqual(normalize_phone_numbers('+1(513)5551234'), '+15135551234')

    def test_filter_cards_with_numbers(self):
        r = filter_cards_with_numbers(self.all_cards, 'cell')
        self.assertEqual(len(r), 2)
